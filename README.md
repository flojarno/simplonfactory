# SimplonFactory : Exercice de programmation orientée objet en Typescript

Application qui permet de créer et de gérer des clients (Customer), de gérer un inventaire de produits (Product) avec des spécificités telles que les vêtements (Clothing) et les chaussures (Shoes), et de gérer des commandes (Order) avec une logique de livraison flexible (Deliverable).

## Configuration
Configuration d'un projet Node : https://www.julienrollin.com/posts/node-typescript-starter/

## Execution dans un terminal
1. Cloner le projet sur votre machine
2. Aller dans le dossier du projet
3. Executer la commande npm run build pour générer le fichier index.js
4. Aller dans /dist
5. Executer la commande node index.js

