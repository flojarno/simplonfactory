import Product from "./Product";
import Customer from "./Customer";
import { Deliverable } from "../interfaces/Deliverable";

export default class Order {

    orderId: number;
    customer: Customer;
    productList: Product[];
    orderDate: Date;
    delivery: Deliverable | undefined;

    constructor(orderId: number, customer: Customer, productList: Product[], orderDate: Date) {
        this.orderId = orderId;
        this.customer = customer;
        this.productList = productList;
        this.orderDate = orderDate;
    }

    // la méthode push() modifie directement le tableau = renvoie le nouveau tableau, sans avoir besoin de le réassigner
    // type de sortie void car la fonction a simplment pour objectif d'ajouter un item a une liste
    addProduct(product: Product): void {
        this.productList.push(product);
    }

    // la méthode filter() crée un nouveau tableau avec tous les éléments qui ont passé le test !== productId, sans modifié le tableau d'origine
    // cette méthode nécessite de réassigner le nouveau tableau
    removeProduct(productId: number): void {
        this.productList = this.productList.filter((product) => product.productId  !== productId)
    }

    // On boucle sur chaque product de la liste, et on assigne le poids du produit courant à la variable accummulateur totalWeight, que l'on retourne
    calculateWeight(): number {
        let totalWeight: number = 0;
        this.productList.forEach((product) => 
            totalWeight += product.weight
        ) 
        return totalWeight;
    }

    // On boucle sur chaque product de la liste, et on assigne le prix du produit courant à la variable accummulateur totalPrice, que l'on retourne
    calculateTotal(): number{
        let totalPrice: number = 0;
        this.productList.forEach((product) =>
            totalPrice += product.price
        )
        return totalPrice;
    }

    setDelivery(delivery: Deliverable){
        this.delivery = delivery;
    }

    calculateShippingCosts(){
        if (this.delivery) { 
            return this.delivery.calculateShippingFee(this.calculateWeight())
        } else {
            console.log("delivery cost calculation impossible")
            return 0;
        }
     
    }

    estimateDeliveryTime(){
    
        if (this.delivery) { 
            return this.delivery.estimateDeliveryTime(this.calculateWeight())
        } else {
            console.log("delivery time calculation impossible")
            return 0;
        }
    }

    // displayOrder(): Affiche les détails de la commande : les informations de l'utilisateur, les informations de chaque produit et le total de la commande.
    displayOrder(){

        // On initialise une variable productDetails avec une chaine vide, qui sera une concatenation des propriétés d'un produit
        let productDetails = '';
        // On boucle sur chaque produit et on fabrique notre chaine de propriétés d'un produit, avec saut de ligne à la fin \n comme séparateur entre chaque produit
        this.productList.forEach((product) =>
          productDetails += `Product ID: ${product.productId}, Name: ${product.name}, Price: ${product.price}€, Weight: ${product.weight}kg, Dimensions: ${product.dimensions.length}cm, ${product.dimensions.width}cm, ${product.dimensions.height}cm\n`
        )
        // Saut de ligne pour chaque ligne 
        return `Order Id: ${this.orderId}\n
        Customer details:\n 
            ID: ${this.customer.customerId}\n 
            Name: ${this.customer.name}\n 
            Email: ${this.customer.email}\n
            Address: ${this.customer.displayAddress()}\n
        Product details: ${productDetails}\n
        Total: ${this.calculateTotal()}€\n
        `
    }
}