import { ShoeSize } from "../enums/ShoeSize";
import { Dimensions } from "../types/Dimensions";
import Product from "./Product";

export default class Shoes extends Product {

    size: ShoeSize;

    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions, size: ShoeSize){
       super(productId, name, weight, price, dimensions);
       this.size = size;
    }

    // surcharge de la méthode parente (override)
    displayDetails(): string {
        return `${super.displayDetails()}, Size: ${this.size}`;
    }
}

//'super' must be called before accessing 'this' in the constructor of a derived class.
// super();