import { Deliverable } from "../interfaces/Deliverable";

export default class StandardDelivery implements Deliverable {

    estimateDeliveryTime(weight: number) {
        if (weight < 10) {
            return 7;
        } else {
            return 10;
        } 
    };

    calculateShippingFee(weight: number) {
        if (weight < 1) {
            return 5;
        } else if ( weight >= 1 && weight <= 5) {
            return 10;
        } else {
            return 20;
        }
    };
}