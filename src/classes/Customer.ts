import { Address } from "../types/Address";

export default class Customer {

    public customerId: number;
    public name: string;
    public email: string;
    public address: Address | undefined;
  
    constructor(customerId: number, name: string, email: string) {
        this.customerId = customerId;
        this.name = name;
        this.email = email;
    }

    displayInfo(): string {
        return `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email}, Address: ${this.displayAddress()}`;
    }

    setAddress(address: Address) {
        this.address = address;
    }

    displayAddress(): string {
        if (this.address){
            return `${this.address.street}, ${this.address.postalCode}, ${this.address.city}, ${this.address.country}`
        } else {
            return "No address found";
        }
    }
}