import { ClothingSize } from "../enums/ClothingSize";
import { Dimensions } from "../types/Dimensions";
import Product from "./Product";

export default class Clothing extends Product {

    size: ClothingSize;

    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions, size: ClothingSize){
        super(productId, name, weight, price, dimensions);
        this.size = size;
    }

    displayDetails(): string {
        return `${super.displayDetails()}, ${this.size}`;
    }
}