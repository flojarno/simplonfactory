import { Dimensions } from '../types/Dimensions';

export default class Product {

    productId: number;
    name: string;
    weight: number;
    price: number;
    dimensions: Dimensions;

    constructor(productId: number, name: string, weight: number, price: number, dimensions: Dimensions){
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.dimensions = dimensions;
    }

    displayDetails(): string {
        return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}kg, Price: ${this.price}€, Dimensions: ${this.dimensions.length}cm, ${this.dimensions.width}cm, ${this.dimensions.height}cm`;
    }
}


