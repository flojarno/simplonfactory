import { Deliverable } from "../interfaces/Deliverable";

export default class ExpressDelivery implements Deliverable {

    estimateDeliveryTime(weight: number) {
        if (weight <= 10) {
            return 1;
        } else {
            return 3;
        } 
    };

    calculateShippingFee(weight: number) {
        if (weight < 1) {
            return 8;
        } else if ( weight >= 1 && weight <= 5) {
            return 14;
        } else {
            return 30;
        }
    };
}

