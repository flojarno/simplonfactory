import Clothing from "./classes/Clothing";
import Customer from "./classes/Customer";
import Order from "./classes/Order";
import Product from "./classes/Product";
import Shoes from "./classes/Shoes";
import StandardDelivery from "./classes/StandardDelivery";
import { ClothingSize } from "./enums/ClothingSize";
import { ShoeSize } from "./enums/ShoeSize";
import { Address } from './types/Address';


// Création d'une instance de Customer
let customer = new Customer(
    1, 
    "Jean", 
    "jean.jean@mail.com"
);

// Création d'un objet Address
let address: Address = {
    street: "123 rue de Rennes",
    city: "Toulouse",
    postalCode: "31500",
    country: "France"
};

// Attribution de l'adresse au client
customer.setAddress(address);

// Affichage des informations du client
console.log(customer.displayInfo());
console.log(customer.displayAddress());
  

// Création d'une instance de Product
let laptop = new Product(1, "PC", 3.2, 900, {length: 15, width: 15, height: 30});

// Création d'une instance de Shoes
let runningShoes = new Shoes(2, "Chaussures de sport", 0.8, 100, {length: 28, width: 10, height: 10}, ShoeSize.Size42);

// Création d'une instance de Clothing
let tshirt = new Clothing(3, "T-shirt", 0.2, 20, {length: 60, width: 50, height: 1}, ClothingSize.M);

// Affichage des détails de chaque produit
console.log(laptop.displayDetails());
console.log(runningShoes.displayDetails());
console.log(tshirt.displayDetails());

// Création d'une commande
let order = new Order(1, customer, [laptop, runningShoes, tshirt], new Date());

// Attribuer un service de livraison à la commande
let standardDelivery = new StandardDelivery();
order.setDelivery(standardDelivery);

// Afficher les détails de la commande
console.log(order.displayOrder());

// Calculer et afficher les coûts de livraison
console.log(`Coûts de livraison: ${order.calculateShippingCosts()}€`);

// Estimer et afficher le délai de livraison
console.log(`Temps de livraison estimé: ${order.estimateDeliveryTime()} days`);