
export enum ClothingSize {

    XS, 
    S, 
    M, 
    L, 
    XL,
    XXL

};