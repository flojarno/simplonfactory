
export type Dimensions = {

    length: number; 
    width: number; 
    height: number;
} 