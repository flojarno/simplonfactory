export type Address = {
    street: string;
    city: string;
    postalCode: string;
    country: string;
} 